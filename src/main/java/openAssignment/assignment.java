package openAssignment;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;

public class assignment {
    WebDriver chrome;
    String url = "https://ecom-test-dat.worldticket.net/";
    String email = "tester@worldticket.net";
    String password = "atm1234";

    @Before
    public void setUpDriver() throws InterruptedException{
        //Set up Chrome Driver.
        WebDriverManager.chromedriver().setup();
        chrome = new ChromeDriver();
        chrome.manage().window().maximize();
        Thread.sleep(1000);

        //1. Open https://ecom-test-dat.worldticket.net/ with Chrome browser.
        chrome.get(url);

        //2. Check Title Name.
        String titleName = chrome.getTitle();
        if (titleName.contains("WorldTicket")){
            System.out.println("Title Correct");
        }
    }

    @Test
    public void actionWorldTicket() throws InterruptedException{

        //3. Click Profile button.
        chrome.findElement(By.xpath("//*[@id=\"header-account\"]/a/i")).click();
        System.out.println("Click profile button success");
        Thread.sleep(1000);

        //4. Input email and password into field.
        chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/form/div/div[2]/div/ul/li[1]/div/input")).sendKeys(email);
        Thread.sleep(1000);
        chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/form/div/div[2]/div/ul/li[2]/div/input")).sendKeys(password);
        Thread.sleep(1000);
        System.out.println("Input Success");

        //Click Login button.
        chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/div[2]/div[1]/div/div/div/form/div/div[2]/div/div[3]/div/button")).click();
        System.out.println("Login Success");
        Thread.sleep(1000);

        //5. Check Username contains Junior Tester in username.
        String name = chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[3]/div[1]/div[2]/div[2]")).getText();
        if(name.contains("Junior Tester")){
            System.out.println("Username Correct.");
        }
        Thread.sleep(1000);

        //Click close pop-up
        chrome.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/a")).click();
        System.out.println("Click close pop-up");

        //6. Click Account Information.
        Thread.sleep(1000);
        chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[3]/div[3]/div/ul/li[5]/a")).click();
        System.out.println("Click Account Information Success");

        //7. Check email in email field is equal login email.
        String emailAddress = chrome.findElement(By.xpath("//*[@id=\"email\"]")).getAttribute("value");
        if (email.equals(emailAddress)){
            System.out.println("Email Correct");
        }else{
            System.out.println("Fail");
        }

        //8. Print active booking information to console put
        chrome.findElement(By.xpath("//*[@id=\"flight_booking\"]/a")).click();
        System.out.println("Click Active Booking");


        String pnrCode = chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[1]")).getText();

        //Print departure
        String departTime = chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[2]")).getText();
        String depart = chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[4]/div")).getText();


        //Print Arrival
        String arrivalTime = chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[3]")).getText();
        String arrival = chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[5]/div")).getText();

        //Print Passenger
        String passenger = chrome.findElement(By.xpath("/html/body/div[3]/div[1]/div/div[2]/div[2]/div/div[2]/div/table/tbody/tr/td[6]")).getText();

        System.out.println("- " + pnrCode
                + "\n" + "\n- " + depart  +  " - " + departTime
                + "\n" + "\n- " + arrival + " - " + arrivalTime
                + "\n" + "\n- " + passenger);
    }

    @After
    public void closeBrowser(){
        chrome.close();
    }
}
